import React,{Component} from 'react'

class RobotForm extends Component {
    constructor(props) {
        super(props)

        this.state  = {
            name: "test_name",
            type: "test_type",
            mass: "test_mass"
        }

        this.add = () => {
            

            props.onAdd({name: this.state.name, type: this.state.type, mass: this.state.mass})
        }
    }

    render() {
        return <div> 

            <input type="text" id="name" onChange={(e) => {this.setState({name: e.target.value})}}/>
            <input type="text" id="type" onChange={(e) => {this.setState({type: e.target.value})}}/>
            <input type="text" id="mass" onChange={(e) => {this.setState({mass: e.target.value})}}/>
            <input type="button" value='add' onClick={this.add()}/>
             </div>
    }
}

export default RobotForm