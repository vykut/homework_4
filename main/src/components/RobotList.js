import React, { Component } from 'react'
import RobotStore from '../stores/RobotStore'
import Robot from './Robot'
import RobotForm from './RobotForm'

class RobotList extends Component {
	constructor(){
		super()

		this.state = {
			robots : []
		}

		this.add = this.add.bind(this)
		this.store = new RobotStore()
	}

	componentDidMount(){
		
		this.setState({
			robots : this.store.getRobots()
		})

		this.store.emitter.addListener('UPDATE', () => {
			this.setState({
				robots : this.store.getRobots()
			})		
		})		
	}

	add = (robot) => {
		console.log(robot)
		if(robot != undefined && robot.name != undefined && robot.type != undefined && robot.mass != undefined) {
			console.log(robot)
			this.store.addRobot(robot)
		}
	}

	render() {
		return (
			<div>
				 <RobotForm onAdd={this.add}/>
				{
					this.state.robots.map((e, i) => 
						<Robot item={e} key={i} />
					)
				}
			</div>
		)
	}
}

export default RobotList
